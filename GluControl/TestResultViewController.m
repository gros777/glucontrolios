//
//  TestResultViewController.m
//  GluControl
//
//  Created by Victor A. Hernández on 2/21/14.
//  Copyright (c) 2014 victorhernandez.me. All rights reserved.
//

#import "TestResultViewController.h"
#import "TestResult.h"

@interface TestResultViewController ()

@end

@implementation TestResultViewController
{
    NSDate *_dateLecture;
    BOOL _datePickerVisible;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if (self.resultToEdit != nil) {
        self.title = @"Edit Test Result";
        _dateLecture = self.resultToEdit.lectureDate ;
        self.lectureNotes.text = self.resultToEdit.lectureNotes;
        self.lectureValue.text = [NSString stringWithFormat:@"%d", self.resultToEdit.lectureValue];
        self.prandialSwitch.on = self.resultToEdit.prandial;
        self.doneBarButton.enabled = YES;
    }else{
        _dateLecture = [NSDate date];
        self.prandialSwitch.on = NO;
    }
    
    [self updateDueDateLabel];
    
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self.lectureValue becomeFirstResponder];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Data table Section

-(NSInteger) tableView:(UITableView *)tableView indentationLevelForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 1 && indexPath.row == 1) {
        NSIndexPath *newIndexPath = [NSIndexPath indexPathForRow:0 inSection:indexPath.section];
        return [super tableView:tableView indentationLevelForRowAtIndexPath:newIndexPath];
    }else{
        return [super tableView:tableView indentationLevelForRowAtIndexPath:indexPath];
    }
}

-(NSIndexPath *) tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 1 && indexPath.row == 0) {
        return indexPath;
    }else {
        return nil;
    }
}

-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
    [self.lectureValue resignFirstResponder];
    if (indexPath.section == 1 && indexPath.row == 0) {
        if(!_datePickerVisible){
            [self showDatePicker];
        }else {
            [self hideDatePicker];
        }
    }
}

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 1 && indexPath.row == 1) {
        return 217.0f;
    }else{
        return [super tableView:tableView heightForRowAtIndexPath:indexPath];
    }
}

-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 1 && _datePickerVisible) {
        return 2;
    }else{
        return [super tableView:tableView numberOfRowsInSection:section];
    }
}

-(UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 1 && indexPath.row == 1) {
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"DatePickerCell"];
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"DatePickerCell"];
            
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            
            UIDatePicker *datePicker = [[UIDatePicker alloc]
                                        initWithFrame:CGRectMake(0.0f, 0.0f, 320.0f, 216.0f)];
            datePicker.tag = 100;
            [cell.contentView addSubview:datePicker];
            [datePicker addTarget:self action:@selector(dateChanged:)
                 forControlEvents:UIControlEventValueChanged];
        }
        return cell;
        
    }else {
        return [super tableView:tableView cellForRowAtIndexPath:indexPath];
    }
}

#pragma mark - Helpers Section

-(void)dateChanged:(UIDatePicker *)datePicker
{
    _dateLecture = datePicker.date;
    [self updateDueDateLabel];
}

- (void)updateDueDateLabel
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateStyle:NSDateFormatterMediumStyle];
    [formatter setTimeStyle:NSDateFormatterShortStyle];
    self.lectureDate.text = [formatter stringFromDate:_dateLecture];
}

- (void)showDatePicker
{
    _datePickerVisible = YES;
    
    NSIndexPath *indexPathDateRow = [NSIndexPath indexPathForRow:0 inSection:1];
    
    NSIndexPath *indexPathDatePicker = [NSIndexPath indexPathForRow:1 inSection:1];
    
    UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:indexPathDateRow];
    cell.detailTextLabel.textColor = cell.detailTextLabel.tintColor;
    
    [self.tableView beginUpdates];
    [self.tableView insertRowsAtIndexPaths:@[indexPathDatePicker]
                          withRowAnimation:UITableViewRowAnimationFade];
    [self.tableView reloadRowsAtIndexPaths:@[indexPathDateRow]
                          withRowAnimation:UITableViewRowAnimationNone];
    [self.tableView endUpdates];
    
    UITableViewCell *datePickerCell = [self.tableView cellForRowAtIndexPath:indexPathDatePicker];
    UIDatePicker *datePicker = (UIDatePicker *) [datePickerCell viewWithTag:100];
    
    [datePicker setDate:_dateLecture animated:NO];
}

- (void) hideDatePicker
{
    if (_datePickerVisible) {
        _datePickerVisible = NO;
        
        NSIndexPath *indexPathDateRow = [NSIndexPath indexPathForRow:0 inSection:1];
        
        NSIndexPath *indexPathDatePicker = [NSIndexPath indexPathForRow:1 inSection:1];
        
        UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:indexPathDateRow];
        cell.detailTextLabel.textColor = [UIColor colorWithWhite:0.0f alpha:0.5f];
        
        [self.tableView beginUpdates];
        [self.tableView reloadRowsAtIndexPaths:@[indexPathDateRow]
                              withRowAnimation:UITableViewRowAnimationNone];
        [self.tableView deleteRowsAtIndexPaths:@[indexPathDatePicker]
                              withRowAnimation:UITableViewRowAnimationFade];
        [self.tableView endUpdates];
    }
}

#pragma mark - Actions section

- (IBAction)done:(id)sender {
    
    if (self.resultToEdit != nil) {
        self.resultToEdit.lectureValue = self.lectureValue.text.intValue;
        self.resultToEdit.lectureNotes = self.lectureNotes.text;
        self.resultToEdit.lectureDate = _dateLecture;
        self.resultToEdit.prandial = self.prandialSwitch.on;
        
        [self.delegate testResultViewController:self didFinishEditingTest:self.resultToEdit];
    }else{
        TestResult *testResult = [[TestResult alloc] init];
        testResult.lectureValue = self.lectureValue.text.intValue;
        testResult.lectureNotes = self.lectureNotes.text;
        testResult.lectureDate = _dateLecture;
        testResult.prandial = self.prandialSwitch.on;
        
        [self.delegate testResultViewController:self didFinishAddingTest:testResult];
    }
}

- (IBAction)cancel:(id)sender {
    [self.delegate testResultViewControllerDidCancel:self];
}

#pragma mark - UITextFieldDelegate Method section

-(BOOL) textField:(UITextField *) theTextField
shouldChangeCharactersInRange:(NSRange)range
replacementString:(NSString *)string
{
    NSString *newText = [theTextField.text stringByReplacingCharactersInRange:range withString:string];
    
    self.doneBarButton.enabled = ([newText length] > 0);
    
    
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    [self hideDatePicker];
}

#pragma mark - UITextViewDelegate methods

- (void) textViewDidBeginEditing:(UITextView *)textView
{
    [self hideDatePicker];
}
@end
