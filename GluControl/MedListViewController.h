//
//  MedListViewController.h
//  GluControl
//
//  Created by Victor A. Hernández on 2/22/14.
//  Copyright (c) 2014 victorhernandez.me. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MedListViewController : UITableViewController

@end
