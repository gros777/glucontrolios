//
//  main.m
//  GluControl
//
//  Created by Victor A. Hernández on 2/21/14.
//  Copyright (c) 2014 victorhernandez.me. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "GluControlAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([GluControlAppDelegate class]));
    }
}
