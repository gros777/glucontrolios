//
//  GlucoseResults.m
//  GluControl
//
//  Created by Victor A. Hernández on 2/22/14.
//  Copyright (c) 2014 victorhernandez.me. All rights reserved.
//

#import "GlucoseResults.h"

@implementation GlucoseResults

- (id)init
{
    if ((self = [super init])) {
        self.results = [[NSMutableArray alloc] initWithCapacity:20];
    }
    
    return self;
}

- (id) initWithCoder:(NSCoder *)aDecoder
{
    if ((self = [super init])) {
        self.results = [aDecoder decodeObjectForKey:@"Results"];
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    [aCoder encodeObject:self.results forKey:@"Results"];
}

- (void)sortResults
{
    [self.results sortUsingSelector:@selector(compare:)];
}

@end
