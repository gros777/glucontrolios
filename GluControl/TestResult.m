//
//  TestResult.m
//  GluControl
//
//  Created by Victor A. Hernández on 2/21/14.
//  Copyright (c) 2014 victorhernandez.me. All rights reserved.
//

#import "TestResult.h"

@implementation TestResult

- (id) initWithCoder:(NSCoder *)aDecoder
{
    if ((self = [super init])) {
        self.lectureValue = [aDecoder decodeIntegerForKey:@"LectureValue"];
        self.lectureDate = [aDecoder decodeObjectForKey:@"LectureDate"];
        self.lectureNotes = [aDecoder decodeObjectForKey:@"LectureNotes"];
        self.prandial = [aDecoder decodeBoolForKey:@"Prandial"];
    }
    
    return  self;
}

- (void) encodeWithCoder:(NSCoder *)aCoder
{
    [aCoder encodeInteger:self.lectureValue forKey:@"LectureValue"];
    [aCoder encodeObject:self.lectureDate forKey:@"LectureDate"];
    [aCoder encodeObject:self.lectureNotes forKey:@"LectureNotes"];
    [aCoder encodeBool:self.prandial forKey:@"Prandial"];
}

- (BOOL) testResultInRange
{
    if (!self.prandial) {
        return !(self.lectureValue > 110);
    } else {
        return !(self.lectureValue > 120);
    }
}

- (BOOL)testResultCritical
{
    return self.lectureValue > 180;
}

- (NSComparisonResult)compare:(TestResult *) otherTestResult
{
    return [self.lectureDate compare:otherTestResult.lectureDate];
}
@end
