//
//  MainMenuViewController.h
//  GluControl
//
//  Created by Victor A. Hernández on 2/22/14.
//  Copyright (c) 2014 victorhernandez.me. All rights reserved.
//

#import <UIKit/UIKit.h>

@class DataModel;

@interface MainMenuViewController : UITableViewController

@property (nonatomic, strong) DataModel *dataModel;

@end
