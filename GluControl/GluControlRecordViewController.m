//
//  GluControlRecordViewController.m
//  GluControl
//
//  Created by Victor A. Hernández on 2/21/14.
//  Copyright (c) 2014 victorhernandez.me. All rights reserved.
//

#import "GluControlRecordViewController.h"

#import "TestResult.h"
#import "GlucoseResults.h"

@interface GluControlRecordViewController ()

@end

@implementation GluControlRecordViewController

- (void)viewDidLoad
{
    [super viewDidLoad];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.glucoseResults.results count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"RecordCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    TestResult *testResult = self.glucoseResults.results[indexPath.row];
    
    [self configureTextForCell:cell withTestResult:testResult];
    
    return cell;
}

- (void) tableView:(UITableView *)tableView
commitEditingStyle:(UITableViewCellEditingStyle)editingStyle
 forRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.glucoseResults.results removeObjectAtIndex:indexPath.row];
    
    NSArray *indexPaths = @[indexPath];
    
    [tableView deleteRowsAtIndexPaths:indexPaths
                     withRowAnimation:UITableViewRowAnimationAutomatic];
}

- (void)configureTextForCell:(UITableViewCell *)cell
              withTestResult:(TestResult *)testResult
{
    NSDateFormatter *formater = [[NSDateFormatter alloc] init];
    [formater setDateStyle:NSDateFormatterMediumStyle];
    [formater setTimeStyle:NSDateFormatterShortStyle];
    
    UILabel *mainText = (UILabel *)[cell viewWithTag:2001];
    mainText.text = [NSString stringWithFormat:@"%d mg/dl", testResult.lectureValue];
    
    UILabel *dateLabel = (UILabel *)[cell viewWithTag:2002];
    dateLabel.text = [formater stringFromDate:testResult.lectureDate];
    
    UILabel *prandialLabel = (UILabel *)[cell viewWithTag:2003];
    prandialLabel.hidden = !testResult.prandial;
    
    if (![testResult testResultInRange]) {
        cell.backgroundColor = [testResult testResultCritical] ? [UIColor redColor]: [UIColor orangeColor];
    }else {
        cell.backgroundColor = [UIColor whiteColor];
    }
    
}
/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/


#pragma mark - Navigation

// In a story board-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"AddTestResult"]) {
        UINavigationController *navigationController = segue.destinationViewController;
        TestResultViewController *controller =
            (TestResultViewController*) navigationController.topViewController;
        controller.delegate = self;
        controller.resultToEdit = nil;
    }
}

- (void)tableView:(UITableView *)tableView
accessoryButtonTappedForRowWithIndexPath:(NSIndexPath *)indexPath
{
    //TestResultNavigationController
    UINavigationController *navigationController = [self.storyboard instantiateViewControllerWithIdentifier:@"TestResultNavigationController" ];
    TestResultViewController *controller =
    (TestResultViewController*) navigationController.topViewController;
    
    controller.delegate = self;
    controller.resultToEdit = self.glucoseResults.results[indexPath.row];
    
    [self presentViewController:navigationController animated:YES completion:nil];
    
}

#pragma mark -TestResultViewControllerDelegate

- (void)testResultViewControllerDidCancel:(TestResultViewController *) controller{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)testResultViewController:(TestResultViewController *) controller
             didFinishAddingTest:(TestResult *) testResult{
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:self.glucoseResults.results.count inSection:0];
    NSArray *indexPaths = @[indexPath];
    
    [self.glucoseResults.results addObject:testResult];
    
    [self.glucoseResults sortResults];
    
    //[self.tableView insertRowsAtIndexPaths:indexPaths
    //                      withRowAnimation:UITableViewRowAnimationAutomatic];
    [self.tableView reloadData];
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)testResultViewController:(TestResultViewController *) controller
            didFinishEditingTest:(TestResult *) testResult{
    
    NSInteger row = [self.glucoseResults.results indexOfObject:testResult];
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:row inSection:0];
    
    UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
    
    [self configureTextForCell:cell withTestResult:testResult];
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
