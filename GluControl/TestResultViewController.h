//
//  TestResultViewController.h
//  GluControl
//
//  Created by Victor A. Hernández on 2/21/14.
//  Copyright (c) 2014 victorhernandez.me. All rights reserved.
//

#import <UIKit/UIKit.h>

@class TestResultViewController;
@class TestResult;

@protocol TestResultViewControllerDelegate <NSObject>

- (void)testResultViewControllerDidCancel:(TestResultViewController *) controller;
- (void)testResultViewController:(TestResultViewController *) controller
             didFinishAddingTest:(TestResult *) testResult;
- (void)testResultViewController:(TestResultViewController *) controller
             didFinishEditingTest:(TestResult *) testResult;

@end


@interface TestResultViewController : UITableViewController <UITextFieldDelegate, UITextViewDelegate>
@property (weak, nonatomic) id<TestResultViewControllerDelegate> delegate;
@property (strong, nonatomic) TestResult *resultToEdit;
@property (weak, nonatomic) IBOutlet UITextField *lectureValue;
@property (weak, nonatomic) IBOutlet UITextView *lectureNotes;
@property (weak, nonatomic) IBOutlet UILabel *lectureDate;
@property (weak, nonatomic) IBOutlet UISwitch *prandialSwitch;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *doneBarButton;


- (IBAction)done:(id)sender;
- (IBAction)cancel:(id)sender;

@end
