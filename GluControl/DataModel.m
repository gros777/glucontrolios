//
//  DataModel.m
//  GluControl
//
//  Created by Victor A. Hernández on 2/22/14.
//  Copyright (c) 2014 victorhernandez.me. All rights reserved.
//

#import "DataModel.h"
#import "GlucoseResults.h"

@implementation DataModel

- (id)init
{
    if ((self = [super init])) {
        [self loadGlucontrolData];
    }
    return self;
}

- (NSString *)documentsDirectory
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documenstDirectory = [paths firstObject];
    return documenstDirectory;
}

- (NSString *) dataFilePath
{
    return [[self documentsDirectory]
            stringByAppendingPathComponent:@"GluControlDataModel.plist"];
}

-(void)saveGlucontrolData
{
    NSMutableData *data = [[NSMutableData alloc] init];
    NSKeyedArchiver *archiver = [[NSKeyedArchiver alloc] initForWritingWithMutableData:data];
    [archiver encodeObject:self.glucoseResults forKey:@"GlucoseResults"];
    [archiver finishEncoding];
    [data writeToFile:[self dataFilePath] atomically:YES];
}

-(void)loadGlucontrolData
{
    NSString *path = [self dataFilePath];
    if ([[NSFileManager defaultManager] fileExistsAtPath:path]) {
        NSData *data = [[NSData alloc] initWithContentsOfFile:path];
        NSKeyedUnarchiver *unarchiver = [[NSKeyedUnarchiver alloc] initForReadingWithData:data];
        self.glucoseResults = [unarchiver decodeObjectForKey:@"GlucoseResults"];
        [unarchiver finishDecoding];
    } else {
        self.glucoseResults = [[GlucoseResults alloc] init];
    }
}

@end
