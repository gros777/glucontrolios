//
//  DataModel.h
//  GluControl
//
//  Created by Victor A. Hernández on 2/22/14.
//  Copyright (c) 2014 victorhernandez.me. All rights reserved.
//

#import <Foundation/Foundation.h>

@class GlucoseResults;

@interface DataModel : NSObject

@property (nonatomic, strong) GlucoseResults *glucoseResults;

-(void)saveGlucontrolData;

@end
