//
//  TestResult.h
//  GluControl
//
//  Created by Victor A. Hernández on 2/21/14.
//  Copyright (c) 2014 victorhernandez.me. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TestResult : NSObject <NSCoding>

@property (nonatomic, assign) NSInteger lectureValue;
@property (nonatomic, copy) NSDate *lectureDate;
@property (nonatomic, copy) NSString *lectureNotes;
@property (nonatomic, assign) BOOL prandial;

- (BOOL) testResultInRange;
- (BOOL)testResultCritical;

@end
