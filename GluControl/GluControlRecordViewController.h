//
//  GluControlRecordViewController.h
//  GluControl
//
//  Created by Victor A. Hernández on 2/21/14.
//  Copyright (c) 2014 victorhernandez.me. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TestResultViewController.h"

@class GlucoseResults;

@interface GluControlRecordViewController : UITableViewController <TestResultViewControllerDelegate>

@property (nonatomic, strong)GlucoseResults *glucoseResults;

@end
