//
//  GlucoseResults.h
//  GluControl
//
//  Created by Victor A. Hernández on 2/22/14.
//  Copyright (c) 2014 victorhernandez.me. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GlucoseResults : NSObject <NSCoding>

@property (nonatomic, strong) NSMutableArray *results;

- (void)sortResults;

@end
